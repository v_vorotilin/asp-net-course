﻿using System;

namespace MyListApp {
    class Program {
        public delegate void MyAction<in T>(T obj);
        
        static void Main(string[] args) {
            Console.WriteLine("Hello World!");
            
            var list = new MyListOfDoubleValues<int>(1, 2, 3, 4, 5, 6, 7);

            var filtered = list.Filter(i => i % 2 == 0);
            
//            foreach (var item in filtered) {
//                Console.WriteLine(item);
//            }
            
//            foreach (var item in list) {
//                Console.WriteLine(item);
//            }

            SortTest();
        }

        private static void SortTest() {
            var test = new MyListOfDoubleValues<int>(3, 50, 1, 36, 83, 20, 2);
            
            test.Sort((v1, v2) => v1 > v2);

            foreach (int value in test) {
                Console.WriteLine(value);
            }
        }
    }
}