using System;
using System.Collections;
using System.Collections.Generic;

namespace MyListApp {
    public class MyListOfDoubleValues<T> : IEnumerator<T>, IEnumerable<T> {
        private readonly T[] _items;
        private int _currentIndex;

        public MyListOfDoubleValues(params T[] items) {
            _items = items;
            _currentIndex = -1;
            
            new List<int>().Sort();
        }

        public void Foreach(Action<T> action) {
            
        }

        public void Sort(Func<T, T, bool> comparator) {
            for (int i = 0; i < _items.Length; i++) {
                for (int j = 0; j < _items.Length; j++) {
                    if (i >= j)
                        continue;

                    if (comparator(_items[i], _items[j])) {
                        var temp = _items[i];
                        _items[i] = _items[j];
                        _items[j] = temp;
                    }
                }
            }
        }

        public IEnumerable<T> Filter(Predicate<T> predicate) {
            foreach (var item in _items) {
                if (predicate(item))
                    yield return item;
            }
        }

        public IEnumerator<T> GetEnumerator() => this;

        public bool MoveNext() =>
            ++_currentIndex < _items.Length;

        public void Reset() {
            _currentIndex = -1;
        }

        T IEnumerator<T>.Current => _items[_currentIndex];

        public object Current => _items[_currentIndex];
        
        public void Dispose() {
            foreach (var item in _items) {
                if (item is IDisposable disposable) {
                    disposable.Dispose();
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }
    }
}