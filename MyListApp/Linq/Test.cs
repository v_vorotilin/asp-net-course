using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq {
    public class Test {

        public static void TestLazy() {
            var ar = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8 };

            var even = ar.Skip(2)
                         .Take(5)
                         .Where(a => {
                                    Console.WriteLine($"Where {a}");
                                    return a % 2 == 0;
                                });
            
            ar.RemoveAt(1);

            foreach (int e in even) {
                Console.WriteLine(e);
            }
        }

        private IEnumerable<int> MyWhere(IEnumerable<int> collection,
                                         Func<int, bool> predicate) {
            foreach (int i in collection) {
                if (predicate(i))
                    yield return i;
            }
        }
        
    }
}