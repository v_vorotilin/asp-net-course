﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq {

    class Animal {
        public int Id;
        public string Name;
        public double Height;
        public double Length;
    }
    
    class Program {
        static void Main(string[] args) {
            Exercise.Do();
            
            return;
            
            Test.TestLazy();
            
            return;
            
            Console.WriteLine("LINQ!");

            var animals = new Animal[] {
                new Animal { Id = 1, Name = "Dog", Length = 100, Height = 50 },
                new Animal { Id = 2, Name = "Cat", Length = 30, Height = 20 },
                new Animal { Id = 3, Name = "Elephant", Length = 600, Height = 400 },
                new Animal { Id = 4, Name = "Tiger", Length = 150, Height = 75 },
            };

            var s = "12,23,45,12,46,30";

            Console.WriteLine(s.Split(',')
                               .Select(i => $"User {i}")
                               .Aggregate((s1, s2) => $"{s1}\n{s2}"));

            var tallAnimals = animals.Where(a => a.Height >= 50)
                                     .Select(a => (a.Name, a.Height))
                                     .OrderBy(a => a)
                                     .Aggregate((previous, current) => 
                                                    (previous.Item1 + ", " + current.Item1,
                                                     previous.Item2 + current.Item2));
            
            Console.WriteLine(tallAnimals);
            
            var dict = new Dictionary<int, string> {
                { 1, "one" },
                { 2, "two" },
                { 3, "three" }
            };

            var all = from l in dict
                      where l.Key % 2 == 0
                      orderby l.Value descending 
                      select l.Value;

//            foreach (var a in all) {
//                Console.WriteLine(a);
//            }

            var list = new List<int> { 10, 20, 30, 40, 50, 60, 70 };

            var filtered = list.Where(l => l % 20 == 0)
                               .OrderByDescending(l => l)
                               .ToDictionary(l => l, l => "value " + l);

            foreach (var f in filtered) {
                Console.WriteLine(f);
            }
        }
    }
}