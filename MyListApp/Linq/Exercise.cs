using System;
using System.Linq;

namespace Linq {
    public class Exercise {
        private const string TestStr = "12:23;1:32;6:54;3:44;10:01";

        public static void Do() {
            var t = TestStr.Split(';')
                           .Select(s => {
                                       var s2 = s.Split(':');
                                       return (int.Parse(s2[0]), int.Parse(s2[1]));
                                   })
                           .Aggregate((t1, t2) => {
                                          var i1 = t1.Item1 + t2.Item1;
                                          var i2 = t1.Item2 + t2.Item2;

                                          return (i1 + i2 / 60, i2 % 60);
                                      });
            
            Console.WriteLine(t);
        }
    }
}