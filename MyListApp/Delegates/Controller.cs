using System;

namespace Delegates {
    public class Controller {
        private Window _window;
        
        public Controller(Window window) {
            _window = window;
            
            _window.Clicked += (x, y) => Console.WriteLine($"CLICK ({x}, {y})");
            _window.DoubleClicked += (x, y) => Console.WriteLine($"DOUBLE click ({x}, {y})");
        }

        private void OnWindowDoubleClicked(int x, int y) {
            var s = "10,20,30,40,50,60";

            s.Trim()
             .Remove(0)
             .AddDot()
             .Substring(5);
        }



        private void OnWindowClicked(int x, int y) {
            Console.WriteLine($"CLICK ({x}, {y})");
        }
    }
}

public static class StringExtensions {
    public static void SplitAndWrite(this string s) {
        var ss = s.Split(',');

        foreach (string s1 in ss) {
            Console.WriteLine(s1);
        }
    }

    public static string AddDot(this string s) => "." + s;
}