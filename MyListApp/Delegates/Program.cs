﻿using System;

namespace Delegates {
    class Program {
        static void Main(string[] args) {
            var s = Console.ReadLine();

            int count;
            
            if (!int.TryParse(s, out count)) 
                throw new InvalidOperationException();
            
            var window  = new Window();
            var controller = new Controller(window);
            
            window.Click(count);
        }
    }
}