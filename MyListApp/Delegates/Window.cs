using System;

namespace Delegates {
    public delegate void ActPoint(int x, int y);
    public delegate int FooPoint(int x, int y);
    
    public class Window {
        public event ActPoint Clicked;
        public event ActPoint DoubleClicked;

        public void Click(int count) {
            switch (count) {
                case 0:
                    break;
                case 1:
                    Clicked?.Invoke(10, 20);
                    break;
                case 2:
                    if (DoubleClicked != null)
                        DoubleClicked(20, 30);
                    break;
                default:
                    throw new InvalidOperationException();
            }
        }
    }
}